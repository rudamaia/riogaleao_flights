<?php

namespace Drupal\riogaleao_flights\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Responds with Flight data from cache.
 */
class Flight extends ControllerBase
{
  /**
   * Get one Flight from cache.
   */
  function get(string $type, string $flightid, Request $request)
  {
    $cacheBackend = \Drupal::service('cache.default');

    if($type != "departure" && $type != "arrival") {
      return new JsonResponse( 'invalid flight type' );
    }

    $type = 'flight_' . $type . '_';
    $cid = $type . $flightid;

    $flight = $cacheBackend->get($cid);

    $response['data'] = array($cid => $flight->data);
    $response['method'] = 'GET';

    return new JsonResponse($response);
  }

  /**
   * Get ALL Flights from cache.
   */
  function all(Request $request)
  {
    return $this->data(25, $request);
  }

  function summary(Request $request)
  {
    return $this->data(3, $request);
  }

  function data($total = 25, Request $request)
  {
    $cacheBackend = \Drupal::service('cache.default');
    $index = $cacheBackend->get('flight_index');
    $flights = [];

    if (false === $index) {
      \Drupal::service('riogaleao_flights.cache')->setFlights();

      $index = $cacheBackend->get('flight_index');
    }

    $airlinesNids = \Drupal::entityQuery('node')->condition('type', 'airlines')->execute();
    $airlinesNodes = \Drupal\node\Entity\Node::loadMultiple($airlinesNids);
    $airlinesItems = [];

    foreach ($airlinesNodes as $node) {
      $name = $node->title->value;
      $color = $node->field_color->color;

      try {
        $logoID = $node->get('field_logo')->first()->get('entity')->getTarget()->getValue()->thumbnail->getValue()[0]['target_id'];
        $filePath = \Drupal\file\Entity\File::load($logoID);
        $logo = \Drupal\Core\Url::fromUri(file_create_url($filePath->getFileUri()))->toString();
      } catch (\Throwable $th) {
        $logo = '';
      }

      $airlinesItems[$this->slugify($name)] = [
        'name'  => $name,
        'color' => $color,
        'logo'  => $logo
      ];
    }

    unset($airlinesNodes);

    $i = 1;

    foreach ($index->data as $item) {
      $flight = $cacheBackend->get($item);
      $data = $flight->data;

      $dates = ['DataHoraConfirmada', 'DataHoraCriacao', 'DataHoraPrevista'];

      foreach ($dates as $key) {
        $data[$key] = $this->fdate($data[$key]);
      }

      $id = $this->slugify($data['NomeCiaAerea']);

      if (isset($airlinesItems[$id])) {
        $data['cor'] = $airlinesItems[$id]['color'];
        $data['logo'] = $airlinesItems[$id]['logo'];
      } else {
        $data['cor'] = '';
        $data['logo'] = '';
      }

      array_push($flights, $data);

      if ($i === $total) {
        break;
      }

      $i++;
    }

    return new JsonResponse($flights);
  }

  public function fdate($date){
    $date = str_replace(array('/','Date','(',')'), '', $date);
    $timezone = substr($date, -8);
    $date = substr($date, 0, -8);
    $st = $date;
    $date = new \DateTime("@$st",new \DateTimeZone('UTC'));
    $date->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
    return $date->format('Y-m-d H:i:s');
  }

  public static function slugify($text)
  {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
      return 'n-a';
    }

    return $text;
  }
}
