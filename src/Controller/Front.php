<?php

namespace Drupal\riogaleao_flights\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class Front extends ControllerBase
{
  /**
   * Displays the login form on the homepage and redirects authenticated users.
   */
  public function frontpage() {
    $build = [];
    if ($this->currentUser()->isAnonymous()) {
      return new JsonResponse( '404 - Not found' );
    }
    else {
      if ($this->currentUser()->hasPermission('access content overview')) {
        // Permitted users are directed to the admin content page.
        return $this->redirect('view.content.page_1');
      }
      $build['heading'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This site has no homepage content.'),
      ];
    }
    return $build;
  }

}
