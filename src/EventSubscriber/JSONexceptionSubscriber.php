<?php
namespace Drupal\riogaleao_flights\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class JSONexceptionSubscriber implements EventSubscriberInterface
{
  public function checkForRedirection(GetResponseEvent $event) {
    if(!is_null($event->getRequest()->attributes->get('exception'))) {
      $statuscode = $event->getRequest()->attributes->get('exception')->getStatusCode();
      $message = $event->getRequest()->attributes->get('exception')->getMessage();
      $event->setResponse(new JsonResponse(
        ['status' => $statuscode, 'message' => $message],
        $statuscode,
        ['X-Robots-Tag' => 'noindex, noarchive, nosnippet']
      ));
    }
  }

  public function RemoveXGeneratorOptions(FilterResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->remove('X-Generator');
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    $events[KernelEvents::RESPONSE][] = array('RemoveXGeneratorOptions', -10);
    return $events;
  }
}
