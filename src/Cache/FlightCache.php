<?php

namespace Drupal\riogaleao_flights\Cache;

use Drupal\riogaleao_flights\Controller\FlightController;
use Drupal\redis\Cache\CacheBackendFactory;
use GuzzleHttp\ {
  Client,
  Exception\ClientException,
  Exception\ServerException
};
/**
 * Cache Flight Data on Predis cache backend.
 */
class FlightCache {

  /**
   * @var \Drupal\redis\Cache\CacheBackendFactory
   */
  protected $cacheBackend;

  /**
   * Creates a redis CacheBackendFactory.
   *
   * @param \Drupal\redis\Cache\CacheBackendFactory $cache_backend
   */

  public function __construct(CacheBackendFactory $cache_backend) {
    $this->cacheBackend = $cache_backend;
  }

  /**
   * Save feed data in the cache backend
   */
  public function setFlights() {
    //get real data from feed
    $departure_feed = $this->getFlights('departure');
    $arrival_feed = $this->getFlights('arrival');

    //log errors from FlightController
    if(!is_array($departure_feed) || !is_array($arrival_feed)) {
      \Drupal::logger('riogaleao_flights')->error('FlightCache received false from FlightController');
      return;
    }

    //init new arrays
    $index = array(array('tags' => null, 'cid' => 'flight_index', 'data' => [], 'expire' => REQUEST_TIME + (600)));
    $departure = [];
    $arrival = [];

    //populate new departure array in redis set format
    foreach ($departure_feed as $departure_flight) {
      //complete flight data
      $flight = $this->flightcacheArray($departure_flight, 'departure');
      array_push($departure, $flight);
      //flight index
      $index_item = $flight['cid'];
      array_push($index[0]['data'], $index_item);
    }

    //populate new arrival array in redis set format
    foreach ($arrival_feed as $arrival_flight) {
      //complete flight data
      $flight = $this->flightcacheArray($arrival_flight, 'arrival');
      array_push($arrival, $flight);
      //flight index
      $index_item = $flight['cid'];
      array_push($index[0]['data'], $index_item);
    }

    //set data in the cache backend
    $this->setMultiple($index);
    $this->setMultiple($departure);
    $this->setMultiple($arrival);

    // drupal_set_message(t('Flights has cached'), 'status');

    return;
  }

  /**
   * prepare flight data to be cached.
   */
  function flightcacheArray(array $data, string $flight_type) {
    $expiration = REQUEST_TIME + (600);
    $departure_cid = 'flight_departure';
    $arrival_cid = 'flight_arrival';
    $type = $flight_type == 'departure' ? $departure_cid : $arrival_cid;

    $flight['tags'] = array(0 => $type);

    $flight['data'] = $data;
    $flight['cid'] = $type . '_' . $data['VooId'];
    $flight['expire'] = $expiration;

    return $flight;
  }

  /**
   * set cache for multiple flights.
   */
  function setMultiple(array $items) {
    //define default cache backend
    $cacheBackend = $this->cacheBackend->get('default');

    foreach ($items as $cid => $item) {
      $cacheBackend->set($item['cid'], $item['data'], isset($item['expire']) ? $item['expire'] : CacheBackendInterface::CACHE_PERMANENT, isset($item['tags']) ? $item['tags'] : []);
    }
  }

  /**
   * Get feed data.
   */
  public function getFlights(string $flight_category, $arrival_parameters = null, $departure_parameters = null) {
    //config
    $config = \Drupal::config('riogaleao_flights.config');

    //config vars
    $riogaleao_flights = $config->get('riogaleo_flights_painelvoo');
    $domain = $riogaleao_flights['domain'];
    $arrival_token = $riogaleao_flights['arrival_token'];
    $arrival_path = $riogaleao_flights['arrival_path'];
    $departure_token = $riogaleao_flights['departure_token'];
    $departure_path = $riogaleao_flights['departure_path'];

    //processed vars
    $arrival_path_final = $arrival_path . $arrival_token . ($arrival_parameters ? $arrival_parameters : '/null/null/null');
    $departure_path_final = $departure_path . $departure_token . ($departure_parameters ? $departure_parameters : '/null/null/null');


    //try to get response and return Flights
    try{
      //Guzzle Client
      $client = new Client(['base_uri' => $domain]);
      $response = $client->request('GET', $flight_category == 'departure' ? $departure_path_final : $arrival_path_final);
      $content = json_decode($response->getBody(), true)['ListaVoo'];
      return $content;
    }
    //Exception loging
    catch (ClientException $e) {
      \Drupal::logger('riogaleao_flights')->error($e->getMessage());
    }
    catch(ServerException $e) {
      \Drupal::logger('riogaleao_flights')->error($e->getMessage());
    }
    //defaults false
    return false;
  }
}
